﻿using System.Linq;
using System.Threading.Tasks;
using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.GrpcService;

namespace Otus.Teaching.PromoCodeFactory.GrpcClient
{
    internal class Program
    {
        public static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Customers.CustomersClient(channel);
            
            var customers = await client.GetCustomersAsync(new EmptyRequest());
            var id = customers.Customers.FirstOrDefault()?.Id;

            var customer = await client.GetCustomerAsync(new GetCustomerRequest {Id = id});

            var createRequest = new CreateCustomerRequest
            {
                Email = "a.lozinskyi@gmail.com",
                FirstName = "Alexei",
                LastName = "Lozinski"
            };
            createRequest.PreferenceIds.AddRange(customer.Preferences.Select(p => p.Id).Take(1));
            var customerCreate = await client.CreateCustomerAsync(createRequest);

            var editRequest = new EditCustomerRequest
            {
                Email = "alozinskiy@yandex.ru",
                Id = customerCreate.Id
            };
            var customerUpdate = await client.EditCustomerAsync(editRequest);

            var deleteRequest = new GetCustomerRequest
            {
                Id = customerCreate.Id
            };
            await client.DeleteCustomerAsync(deleteRequest);
        }
    }
}
