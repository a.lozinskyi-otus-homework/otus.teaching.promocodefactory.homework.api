﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.SignalRServer.Models;

namespace Otus.Teaching.PromoCodeFactory.SignalRServer.Hubs
{
    public class CustomersHub : Hub
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersHub(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<List<CustomerShortResponse>> GetCustomers()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return response;
        }

        public async Task<CustomerResponse> GetCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            return CustomerMapper.MapFromModel(customer);
        }

        public async Task<CustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            var customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return CustomerMapper.MapFromModel(customer);
        }

        public async Task<CustomerResponse> EditCustomer(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return null;
            }

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return CustomerMapper.MapFromModel(customer);
        }

        public async Task<string> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return "NotFound";

            await _customerRepository.DeleteAsync(customer);

            return "Deleted";
        }
    }
}
