﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.SignalRServer.Models;

namespace Otus.Teaching.PromoCodeFactory.SignalRServer
{   
    public class CustomerMapper
    {

        public static Customer MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            if (customer == null)
            {
                customer = new Customer
                {
                    Id = Guid.NewGuid()
                };
            }

            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            return customer;
        }

        public static CustomerResponse MapFromModel(Customer customer)
        {
            return new CustomerResponse
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = customer.Preferences.Select(p => 
                    new PreferenceResponse
                    {
                        Id = p.Preference.Id,
                        Name = p.Preference.Name
                    })
                    .ToList(),
            };
        }
    }
}
