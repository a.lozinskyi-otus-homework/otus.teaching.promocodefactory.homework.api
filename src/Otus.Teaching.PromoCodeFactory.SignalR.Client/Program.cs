﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.SignalRServer.Models;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:44311/hubs/customers")
                .WithAutomaticReconnect()
                .Build();

            await connection.StartAsync();
            var customers = await connection.InvokeAsync<CustomerShortResponse[]>("GetCustomers");
            var id = customers.FirstOrDefault()?.Id;

            var customer = await connection.InvokeAsync<CustomerResponse>("GetCustomer", id);

            var createRequest = new CreateOrEditCustomerRequest
            {
                Email = "a.lozinskyi@gmail.com",
                FirstName = "Alexei",
                LastName = "Lozinski",
                PreferenceIds = customer.Preferences.Select(p => p.Id).Take(1).ToList()
            };
            var customerCreate = await connection.InvokeAsync<CustomerResponse>("CreateCustomer", createRequest);

            var editRequest = new CreateOrEditCustomerRequest
            {
                Email = "alozinskiy@yandex.ru",
            };
            var customerUpdate =
                await connection.InvokeAsync<CustomerResponse>("EditCustomer", customerCreate.Id, editRequest);

            var deleteResult = await connection.InvokeAsync<string>("DeleteCustomer", customerUpdate.Id);
            Console.WriteLine(deleteResult);
            Console.ReadLine();
            await connection.StopAsync();
        }
    }
}
