﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.GrpcService
{
    public class CustomerMapper
    {
        public static Customer MapFromModel(CreateCustomerRequest model, IEnumerable<Preference> preferences)
        {
            var customer = new Customer
            {
                Id = Guid.NewGuid(), FirstName = model.FirstName, LastName = model.LastName, Email = model.Email
            };

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            return customer;
        }

        public static Customer MapFromModel(EditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer)
        {
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            return customer;
        }

        public static CustomerReply MapFromModel(Customer customer)
        {
            var customerReply = new CustomerReply
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
            customerReply.Preferences.AddRange(customer.Preferences
                .Select(p => new PreferenceReply
                {
                    Id = p.Preference.Id.ToString(),
                    Name = p.Preference.Name
                }));
            return customerReply;
        }
    }
}