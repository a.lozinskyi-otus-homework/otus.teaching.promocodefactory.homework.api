﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.GrpcService.Services
{
    public class CustomersService : Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomersReply> GetCustomers(EmptyRequest request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();
            var response = new CustomersReply();
            response.Customers.AddRange(customers.Select(c => new CustomerShortReply
            {
                Id = c.Id.ToString(),
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName
            }));

            return response;
        }

        public override async Task<CustomerReply> GetCustomer(GetCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            return CustomerMapper.MapFromModel(customer);
        }

        public override async Task<CustomerReply> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(Guid.Parse).ToList());
            var customer = CustomerMapper.MapFromModel(request, preferences);
            await _customerRepository.AddAsync(customer);
            return CustomerMapper.MapFromModel(customer);
        }

        public override async Task<CustomerReply> EditCustomer(EditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (customer == null)
            {
                return new CustomerReply();
            }

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(Guid.Parse).ToList());
            CustomerMapper.MapFromModel(request, preferences, customer);
            await _customerRepository.UpdateAsync(customer);
            return CustomerMapper.MapFromModel(customer);
        }

        public override async Task<EmptyResponse> DeleteCustomer(GetCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (customer == null)
            {
                return new EmptyResponse();
            }

            await _customerRepository.DeleteAsync(customer);

            return new EmptyResponse();
        }
    }
}
